/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Dulaj Pathirana on 12/9/2019, 12:19 PM
 */

// Logger config
const Logger = require('./api/utils/logger');
const logger = new Logger('App');

const AppServer = require('./api/app/server');
const config = require('./api/config/config');

const appServer = new AppServer();
const port = process.env.port || config.get('/port') || 8080;

appServer.server.listen(port, () => {

    logger.info('Your server is listening on port %d (http://localhost:%d)', port, port);
    logger.info('Swagger-ui is available on http://localhost:%d/docs', port);
});
