/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Dulaj Pathirana on 19/9/2019, 3:24 PM
 */


const availabilityService = require('../api/services/packageAvailabilityService');
const Auth = require('../api/services/clientAuthorizationService');
const availabilityStatus = require('../api/config/const/availabilityStatus');

const availabilityData = require('./data/packageAvailabilityData');

let chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
const expect = chai.expect;
const assert = chai.assert;

describe('Package Availability', () => {

    describe('PT1', () => {
        it('should provide response with PT1 status', function () {
            this.timeout(100000);
            let latitude = availabilityData.location.pt1.latitude;
            let longitude = availabilityData.location.pt1.longitude;
            let availability = availabilityService.getPackagesAvailability(latitude, longitude, true);

            return availability.then(value => {
                expect(value.status).to.equal(availabilityStatus.PT1.name);
                expect(value.packages).not.to.be.empty;
                expect(value.sto).not.to.be.null;
            });
        });

        it('should provide response with PT1 with no cache', async function () {
            this.timeout(100000);
            let latitude = availabilityData.location.pt1.latitude;
            let longitude = availabilityData.location.pt1.longitude;
            let availability = await availabilityService.getPackagesAvailability(latitude, longitude, false);

            assert.strictEqual(availability.status, availabilityStatus.PT1.name);
        });
    });

    describe('PT2', () => {
        it('should provide response with PT2 status', function () {
            this.timeout(100000);

            let latitude = availabilityData.location.pt2.latitude;
            let longitude = availabilityData.location.pt2.longitude;
            let availability = availabilityService.getPackagesAvailability(latitude, longitude, false);
            return availability.then(value => {
                expect(value.status).to.equal(availabilityStatus.PT2.name);
            });
        });
    }
    );

    describe('PT3', () => {
        it('should provide response with PT3 status', function () {
            this.timeout(100000);
            let latitude = availabilityData.location.pt3.latitude;
            let longitude = availabilityData.location.pt3.longitude;
            let availability = availabilityService.getPackagesAvailability(latitude, longitude, true);
            return availability.then(value => {
                expect(value.status).to.equal(availabilityStatus.PT3.name);
                expect(value.packages).to.be.empty;
            });
        });
    }
    );

    after('closing the cache', () => {
        return availabilityService.closeCache();
    });
}
);

describe('Authorization', () => {
    describe('#getJWTFForUser', () => {
        it('should give the Access Token', () => {
            return new Auth().getJWTFForUser().then(jwt => {
                assert(jwt);
            });
        });
    });
});
