/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Alif Septian
 */

const sinon = require('sinon');
const wrapper = require('../../api/utils/wrapper');

const {
    NotFoundError, InternalServerError, ConflictError, BadRequestError, ForbiddenError, ConditionNotMetError,
    MethodNotAllowedError, UnauthorizedError, ServiceUnavailableError, PreconditionFailedError
} = require('../../api/utils/error');

describe('Wrapper', () => {

    const res = {
        status: () => {
            return {
                send: sinon.stub()
            };
        }
    };

    describe('response', () => {
        it('should cover branch', () => {
            wrapper.response(res, 'success', {data: {}, meta: {}});
        });
    });

    describe('formattedResponse', () => {
        it('ok', () => wrapper.formattedResponse(res, true, {data: {}, error: {}}));
        it('not ok', () => wrapper.formattedResponse(res, false, {data: {}, error: new Error()}));
    });

    describe('checkErrorCode', () => {
        it('should return NotFoundError', () => wrapper.response(res, 'fail', {error: new NotFoundError()}));

        it('should return BadRequestError', () => wrapper.response(res, 'fail', {error: new BadRequestError()}));

        it('should return InternalServerError', () => wrapper.response(res, 'fail', {error: new InternalServerError()}));

        it('should return ConflictError', () => wrapper.response(res, 'fail', {error: new ConflictError()}));

        it('should return GatewayTimeoutError', () => wrapper.response(res, 'fail', {error: new MethodNotAllowedError()}));

        it('should return UnauthorizedError', () => wrapper.response(res, 'fail', {error: new UnauthorizedError()}));

        it('should return ForbiddenError', () => wrapper.response(res, 'fail', {error: new ForbiddenError()}));

        it('should return ServiceUnavailableError', () => wrapper.response(res, 'fail', {error: new ServiceUnavailableError()}));

        it('should return PreconditionFailedError', () => wrapper.response(res, 'fail', {error: new PreconditionFailedError()}));

        it('should return ConditionNotMetError', () => wrapper.response(res, 'fail', {error: new ConditionNotMetError()}));

    });
});
