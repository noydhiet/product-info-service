/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Alif Septian
 */

const assert = require('assert');
const {findByUsername} = require('../../api/routes/middlewares/basicAuth');

describe('Auth Repository', () => {

    describe('findByUsername', () => {
        it('should error invalid password', () => {
            findByUsername('test', user => {
                assert.equal(user.username, undefined);
                assert.equal(user.password, undefined);
            });
        });
    });
});
