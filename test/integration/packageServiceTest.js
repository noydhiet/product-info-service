/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Dulaj Pathirana on 20/11/2019, 5:56 PM
 */

const packagesService = require('../../api/services/packagesService');
let chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const {packageIds, invalidPackageIds} = require('../data/packageData');
chai.use(chaiAsPromised);
const expect = chai.expect;

describe('Package CRUD Operations', () => {

    describe('Read', () => {
        it('should list All the packages', function () {
            this.timeout(100000);
            return packagesService.getPackages(languages.EN, 5)
                .then(packages => {
                    expect(packages.error).to.be.null;
                    expect(packages.data).not.to.be.empty;
                });
        });

        it('should list the package by ID', function () {
            this.timeout(100000);
            return packagesService.getPackages(languages.EN, 5)
                .then(packages => {
                    expect(packages.error).to.be.null;
                    expect(packages.data).not.to.be.empty;
                    let selectedPkg = packages['data'][0];

                    return packagesService.getPackagesById(selectedPkg['id']).then(pkg => {
                        expect(pkg.data).not.to.be.null;
                        expect(pkg.data['id']).to.equal(selectedPkg['id']);
                        expect(pkg.data['clientId']).to.equal(selectedPkg['clientId']);
                    });
                });
        });

        it('should find the best package by price', function () {
            this.timeout(100000);
            return packagesService.getPackages(languages.EN, 5)
                .then(packages => {
                    expect(packages.error).to.be.null;
                    expect(packages.data).not.to.be.empty;
                    let selectedPkg = packages['data'][0];

                    return packagesService.getPackagesById(selectedPkg['id']).then(pkg => {
                        expect(pkg.data).not.to.be.null;
                        expect(pkg.data['id']).to.equal(selectedPkg['id']);
                        expect(pkg.data['clientId']).to.equal(selectedPkg['clientId']);

                        return packagesService.packagesPriceSearch(selectedPkg['price'] * 0.6).then(filtered => {
                            expect(filtered.data).not.to.be.null;
                            expect(filtered.data).not.to.be.empty;
                        });
                    });
                });
        });

        it('should filter the packages based on the id', function () {
            this.timeout(100000);
            return packagesService.filterPackageFromIds(packageIds).then(pkgs => {
                expect(pkgs.data).not.to.be.null;
                expect(pkgs.data).not.to.be.empty;
                expect(pkgs.data.length).to.be.equal(packageIds.length);
            });
        });

        it('should filter the packages based on the invalid id', function () {
            this.timeout(100000);
            return packagesService.filterPackageFromIds(invalidPackageIds).then(pkgs => {
                expect(pkgs.data).not.to.be.null;
                expect(pkgs.data).to.be.empty;
            });
        });

        it('should provide all the packages for empty ids', function () {
            this.timeout(100000);
            return packagesService.filterPackageFromIds([]).then(pkgs => {
                expect(pkgs.data).not.to.be.null;
                expect(pkgs.data).not.to.be.empty;
            });
        });

    });
}
);

