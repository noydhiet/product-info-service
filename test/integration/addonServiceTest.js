/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Dulaj Pathirana on 20/11/2019, 5:56 PM
 */

const addonService = require('../../api/services/addonsService');
let chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
const expect = chai.expect;

describe('Addon CRUD Operations', () =>

    describe('Read', () => {

        it('should list All the Addons', function () {
            this.timeout(100000);
            return addonService.getAddons(5)
                .then(packages => {
                    expect(packages.error).to.be.null;
                    expect(packages.data).not.to.be.empty;
                });
        });

        it('should list the package by ID', function () {
            this.timeout(100000);
            return addonService.getAddonById(5)
                .then(addons => {
                    expect(addons.error).to.be.null;
                    expect(addons.data).not.to.be.empty;
                    let selectedPkg = addons['data'][0];

                    return addonService.getAddonById(selectedPkg['id']).then(addon => {
                        expect(addon.data).not.to.be.null;
                        expect(addon.data['id']).to.equal(selectedPkg['id']);
                        expect(addon.data['clientId']).to.equal(selectedPkg['clientId']);
                    });
                });
        });

    })
);

