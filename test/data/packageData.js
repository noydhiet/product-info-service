/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Dulaj Pathirana on 8/1/2020, 12:33 PM
 */

module.exports = {
    packageIds: ['pkg_0001', 'pkg_0002'],
    invalidPackageIds: ['-100'],
};
