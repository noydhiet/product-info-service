/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Dulaj Pathirana on 20/11/2019, 5:47 PM
 */

module.exports = {

    location: {
        pt1: {
            latitude: '-0.032732',
            longitude: '109.333373'
        },
        pt2: {
            latitude: '-5.76197335604423',
            longitude: '105.11505989464789'
        },
        pt3: {
            latitude: '110.032732',
            longitude: '119.333373'
        }
    }
};

