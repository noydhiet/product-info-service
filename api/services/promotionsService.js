/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Dulaj Pathirana on 21/10/2019, 5:08 PM
 */


/**
 * get Promotion by ID
 *
 * id String authentication header
 * returns inline_response_200_10
 **/
exports.getPromotionByID = (id) => new Promise((resolve, reject) => {
    let examples = {};
    examples['application/json'] = {
        'data': {
            'cilentId': 'cilentId',
            'images': ['images', 'images'],
            'createdDate': 'createdDate',
            'endingDate': 'endingDate',
            'price': 0.8008281904610115,
            'discontedPrice': 6.027456183070403,
            'id': 'id',
            'promotionDetails': [{
                'name': 'name',
                'description': {
                    'image': 'image',
                    'subTitle': 'subTitle',
                    'title': 'title'
                }
            }, {
                'name': 'name',
                'description': {
                    'image': 'image',
                    'subTitle': 'subTitle',
                    'title': 'title'
                }
            }],
            'termsAndConditions': [{
                'text': 'text',
                'title': 'title'
            }, {
                'text': 'text',
                'title': 'title'
            }]
        },
        'message': 'message',
        'ok': true,
        'status': 200
    };
    if (Object.keys(examples).length > 0) {
        resolve(examples[Object.keys(examples)[0]]);
    } else {
        resolve();
    }
});


/**
 * List all the Promotions
 *
 * authorization String authentication header
 * returns inline_response_200_9
 **/
exports.getPromotions = () => new Promise((resolve, reject) => {
    let examples = {};
    examples['application/json'] = {
        'data': [{
            'cilentId': 'cilentId',
            'images': ['images', 'images'],
            'createdDate': 'createdDate',
            'endingDate': 'endingDate',
            'price': 0.8008281904610115,
            'discontedPrice': 6.027456183070403,
            'id': 'id',
            'promotionDetails': [{
                'name': 'name',
                'description': {
                    'image': 'image',
                    'subTitle': 'subTitle',
                    'title': 'title'
                }
            }, {
                'name': 'name',
                'description': {
                    'image': 'image',
                    'subTitle': 'subTitle',
                    'title': 'title'
                }
            }],
            'termsAndConditions': [{
                'text': 'text',
                'title': 'title'
            }, {
                'text': 'text',
                'title': 'title'
            }]
        }, {
            'cilentId': 'cilentId',
            'images': ['images', 'images'],
            'createdDate': 'createdDate',
            'endingDate': 'endingDate',
            'price': 0.8008281904610115,
            'discontedPrice': 6.027456183070403,
            'id': 'id',
            'promotionDetails': [{
                'name': 'name',
                'description': {
                    'image': 'image',
                    'subTitle': 'subTitle',
                    'title': 'title'
                }
            }, {
                'name': 'name',
                'description': {
                    'image': 'image',
                    'subTitle': 'subTitle',
                    'title': 'title'
                }
            }],
            'termsAndConditions': [{
                'text': 'text',
                'title': 'title'
            }, {
                'text': 'text',
                'title': 'title'
            }]
        }],
        'message': 'message',
        'ok': true,
        'status': 200
    };
    if (Object.keys(examples).length > 0) {
        resolve(examples[Object.keys(examples)[0]]);
    } else {
        resolve();
    }
});

