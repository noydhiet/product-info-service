/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Dulaj Pathirana on 12/9/2019, 12:19 PM
 */


const util = require('util');
const request = require('request-promise');
const Logger = require('../utils/logger');
const logger = new Logger('AuthenticationService');
const config = require('../config/config');

const {ServiceUnavailableError} = require('../utils/error');

class ClientAuthorization {

    constructor() {
        this.token = '';
    }

    async getJWTFForUser() {
        if (this.token) {
            logger.info('using the cached gateway access token');
            return this.token;
        }
        logger.info('requesting gateway access token');
        this.token = await this._getJWT();
        setTimeout(() => {
            this.token = '';
        }, config.get('/authorization').timeOut);
        return this.token;
    }


    /**
     * Get access token form the cline API using Basic Authentication
     *
     * user String username
     * secret String password
     **/
    async _getJWT() {

        let options = {
            method: 'GET',
            url: config.get('/authorization').host + config.get('/authorization').endpoint,
            headers:
                {
                    'Authorization': 'Basic ' + config.get('/authorization').token,
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
            json: true,
            strictSSL: false
        };

        let resp = await request(options).catch(e => {
            logger.error('cannot connect to the authentication service', e);
            throw new ServiceUnavailableError('cannot connect to the authentication service');
        });
        return !util.isNullOrUndefined(resp) && !util.isNullOrUndefined(resp.data) ? resp.data['token'] : null;

    }
}

module.exports = ClientAuthorization;
