/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Dulaj Pathirana on 21/10/2019, 5:05 PM
 */


const PackageRepository = require('../repository/packageRepository');

const packageRepo = new PackageRepository();

const config = require('../config/config');
const priceFilterConfig = config.get('/packagesInfo').priceFilter;

const {languages} = require('../config/const/commonConstants');
const packagesLimit = config.get('/elasticSearchIndices').packages.limit;
const validator = require('../utils/validator');
const wrapper = require('../utils/wrapper');
const {packageSchema} = require('../models/products');

const Logger = require('../utils/logger');
const logger = new Logger('PackageService');

/**
 * List all the packages
 * @param language
 * @param limit
 * @returns {Promise<*>}
 **/
module.exports.getPackages = async (language = languages.EN, limit = packagesLimit) => packageRepo.getAllPackages(language, limit);

/**
 * add package
 * @param pkg
 * @param language
 * @returns {Promise<*>}
 */
module.exports.addPackage = async (pkg, language = languages.EN) => {
    let validatedPackage = validator.isValidPayload(pkg, packageSchema);
    if (validatedPackage.error) {
        return wrapper.error(validatedPackage.error);
    }
    return new PackageRepository().addPackage(validatedPackage.data, language);

};

/**
 * add package
 * @param id
 * @param pkg
 * @param language
 * @returns {Promise<*>}
 */
module.exports.updatePackage = async (pkg, id, language = languages.EN) => {
    let validatedPackage = validator.isValidPayload(pkg, packageSchema);
    if (validatedPackage.error) {
        return wrapper.error(validatedPackage.error);
    }
    return packageRepo.updatePackage(validatedPackage.data, id, language);
};

/**
 * get package by ID
 *
 * @param id
 * @param language
 * @returns {Promise<*>}
 * **/
module.exports.getPackagesById = async (id, language = languages.EN) => {
    return packageRepo.getPackageById(id, language);
};

/**
 * Delete Package by ID
 * @param id
 * @param language
 * @returns {Promise<*>}
 */
module.exports.deletePackageById = async (id, language = languages.EN) => {
    return packageRepo.deletePackage(id, language);
};

/**
 *
 * @param {[String]}    needs
 * @param language
 * @returns {Promise<{data: *, error: null} | {data: null, error: *}>}
 */
module.exports.packagesNeedsSearch = async (needs, language = languages.EN) => {
    const needsMatch = {
        terms: {
            //fixme: needs key
            key: needs,
            minimum_should_match: 1
        }
    };
    return await packageRepo.findMatchedProduct(needsMatch, language, null);
};

/**
 *
 * @param {[String]}    ids, if empty all the items will be returned, else only the active packages matched with
 * client id's will be returned
 * @param language
 * @returns {[String]} {Promise<void>}
 */
module.exports.filterPackageFromIds = async (ids, language = languages.EN) => {

    let idMatchQuery = {
        bool: {
            must: [
                {
                    match: {
                        active: true
                    }
                }
            ]
        }
    };

    if (ids && ids.length > 0) {
        idMatchQuery = {
            bool: {
                must: [
                    {
                        match: {
                            active: true
                        }
                    },
                    {
                        terms: {
                            'id.keyword': [...ids]
                        }
                    }
                ]
            }
        };
    }

    return packageRepo.findMatchedProduct(idMatchQuery, language, null);
};

/**
 * Package Price Search
 * Will try to increase the range gradually until results are found for @priceFilterConfig.retries times,
 * by adjusting the upper bound for the price
 * @param price
 * @param language
 * @returns {Promise<*>}
 */
module.exports.packagesPriceSearch = async (price, language = languages.EN) => {

    let upperBound = price * priceFilterConfig.upperBoundRatio;
    return getPackagesInPriceRange(upperBound, priceFilterConfig.retries, language);
};

async function getPackagesInPriceRange(upperBound, retires, language = languages.EN) {
    const priceRangeMatch = {
        range: {
            price: {
                lte: upperBound
            }
        }
    };
    logger.info(`searching prize range as : ${JSON.stringify(priceRangeMatch)}`);
    let packages = await packageRepo.findMatchedProduct(priceRangeMatch, language);
    if (packages.data && packages.data.length > 0) {
        return packages;
    }
    return getPackagesInPriceRange(upperBound * priceFilterConfig.upperBoundRatio, retires - 1, language);

}
