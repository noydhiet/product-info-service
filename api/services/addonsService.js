/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Dulaj Pathirana on 26/11/2019, 9:57 AM
 */

const AddonRepository = require('../repository/addonsRepository');

const addonRepository = new AddonRepository();

const validator = require('../utils/validator');
const wrapper = require('../utils/wrapper');
const {addonUpdateSchema} = require('../models/products');

/**
 * List all the addon
 * @param limit
 * @returns {Promise<*>}
 **/
module.exports.getAddons = async limit => addonRepository.getAllAddons(limit);

/**
 * add addon
 * @param addon
 * @returns {Promise<*>}
 */
module.exports.addAddon = async addon => {
    let validatedPackage = validator.isValidPayload(addon, addonUpdateSchema);
    if (validatedPackage.error) {
        return wrapper.error(validatedPackage.error);
    }
    return new AddonRepository().addAddon(validatedPackage.data);

};

/**
 * add addon
 * @param id
 * @param addon
 * @returns {Promise<*>}
 */
module.exports.updateAddon = async (addon, id) => {
    validator.isValidPayload(addon, addonUpdateSchema);
    return addonRepository.updateAddon(addon, id);
};

/**
 * get addon by ID
 *
 * @param id
 * @returns {Promise<*>}
 * **/
module.exports.getAddonById = async id => {
    return addonRepository.getAddonById(id);
};

/**
 * Delete Addon by ID
 * @param id
 * @returns {Promise<*>}
 */
module.exports.deleteAddonById = async id => {
    return addonRepository.deleteAddon(id);
};
