/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Dulaj Pathirana on 23/12/2019, 5:47 PM
 */

const Logger = require('../utils/logger');
const logger = new Logger('PackageAvailabilityService');

const config = require('../config/config');
const priceFilterConfig = config.get('/profileService').host;
const locationUpdateEndpoint = config.get('/profileService').locationUpdateEndpoint;
const serviceAuth = config.get('/authorization').token;
const {ServiceUnavailableError} = require('../utils/error');
const rp = require('request-promise');

module.exports.saveUserLocationInAccount = async location => {

    const options = {
        method: 'POST',
        uri: `${priceFilterConfig}/${locationUpdateEndpoint}`,
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Basic ${serviceAuth}`,
        },
        json: location,
        strictSSL: false
    };

    logger.info(`sending location data to profile service: ${new Date()} : ${JSON.stringify(location)}`);
    try {
        const result = await rp.post(options);
        logger.info(`got response profile service: ${new Date()} as : ${JSON.stringify(result)}`);

        if (result.ok) {
            return {
                message: 'location saved',
                location: result.data
            };
        }
    } catch (e) {
        logger.error('Account creation error', e);
        throw new ServiceUnavailableError(e.error.message);
    }

    return {
        message: 'location saving error',
        location: null
    };

};
