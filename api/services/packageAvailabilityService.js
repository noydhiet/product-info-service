/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Dulaj Pathirana on 12/9/2019, 12:19 PM
 */


const rp = require('request-promise');

const Logger = require('../utils/logger');
const logger = new Logger('PackageAvailabilityService');

const AuthenticationService = require('./clientAuthorizationService');
const authenticationService = new AuthenticationService();
const userProfileConnector = require('./userProfileServiceConnector');
const packageService = require('./packagesService');
const validator = require('../utils/validator');

const config = require('../config/config');
const {PT1, PT2, PT3} = require('../config/const/availabilityStatus');
const {languages} = require('../config/const/commonConstants');

const {BadRequestError} = require('../utils/error');
const {locationSchema} = require('../models/location');
const {
    Package,
    InternetPackage,
    CallPackage,
    TVPackage,
    TermsAndConditions,
    Addon,
    InternetAddon,
    Description
} = require('../models/products');

const redis = require('redis');
const {promisify} = require('util');
let cache = redis.createClient(config.get('/cache').port, config.get('/cache').host);
cache.getAsync = promisify(cache.get).bind(cache);

const cacheEnabled = config.get('/cache').isEnabled === 'true';
const invalidationTimeout = config.get('/cache').invalidationTimeout;
const nearByAvailabilityApiHost = config.get('/availability').nearByHost;
const inRangeApiHost = config.get('/availability').inRangeHost;
const nearByCableHostApiHost = config.get('/availability').inNearByCableHost;
const listPackagesHost = config.get('/availability').listPackagesHost;

if (cacheEnabled) {
    cache.on('connect', () => {
        logger.info('cache connected');
    });
    cache.on('error', err => {
        logger.error('cache error: ', err);
    });
}


class AvailabilityResponse {
    constructor(found, status, availableIn, source, packages, area, sto, locId, system, deviceId) {
        this.found = found;
        this.status = status;
        this.availableIn = availableIn;
        this.source = source;
        this.packages = packages;
        this.area = area;
        this.sto = sto;
        this.locId = locId;
        this.system = system;
        this.deviceId = deviceId;
    }
}

module.exports.getPackagesAvailabilityAndSaveLocation = async (latitude, longitude, cacheRequired, location, user, language = languages.EN) => {
    const availability = await this.getPackagesAvailability(latitude, longitude, cacheRequired, language);
    let accountConfirmation;
    if (availability.status === PT1.name) {
        if (location.rtrw === 'null' || location.rtrw === null || location.rtrw === '') {
            delete location.rtrw;
        }
        //validate later to enable get availability only for latitude and longitude
        if (validator.isValidPayload(location, locationSchema).error) {
            throw new BadRequestError(validator.isValidPayload(location, locationSchema).error);
        }

        //set system params get from the PT1 availability response
        location.userId = user.userId;
        location.userEmail = user.email;
        location.sto = availability.sto;
        location.deviceId = `${availability.deviceId}`;
        location.system = availability.system;
        location.locId = availability.locId;
        location.addressDesc = location.addressDescription;
        delete location.addressDescription;

        if (location.rtrw) {
            let [rt, rw] = location.rtrw.split('/');
            location.rt = rt;
            location.rw = rw;
            delete location.rtrw;
        }

        accountConfirmation = await userProfileConnector.saveUserLocationInAccount(location);
    }

    return {
        ...availability,
        accountConfirmation
    };

};

/**
 * Find Product Availability for the User Location
 *
 * authorization String basic authentication header(
 * latitude String latitude of the user location
 * longitude String longitude of the user location
 * area String area of the user location (optional)
 * city String city of the user location (optional)
 * no response value expected for this operation
 **/
module.exports.getPackagesAvailability = async (latitude, longitude, cacheRequired, language = languages.EN) => {

    let cacheKey = 'ProductInfo/PackageAvailability/' + latitude + ':' + longitude;
    if (cacheEnabled && cacheRequired) {
        logger.info('cache enabled, searching in the cache');

        let cachedResponse = await cache.getAsync(cacheKey);
        if (cachedResponse) {
            logger.info('cached results exists for: ', cacheKey);
            cachedResponse = JSON.parse(cachedResponse);
            cachedResponse.source = 'cache';
            return cachedResponse;
        }
        logger.info('no cache found, requesting through the API');
        return getAvailablePackagesFromApi(latitude, longitude, cacheKey, language);
    }
    logger.info('cache disabled or not requesting data from the cache. getting data form the API');
    return getAvailablePackagesFromApi(latitude, longitude, cacheKey, language);

};

module.exports.clearCache = () => new Promise((resolve, reject) => {
    if (cacheEnabled) {
        cache.flushall('ASYNC', (err) => {
            if (err) {
                reject('cache clear error');
            } else {
                resolve('cache cleared successfully');
            }
        });
    }
});

/**
 *
 * @param latitude
 * @param longitude
 * @param cacheKey
 * @param language
 * @returns {Promise<AvailabilityResponse>}
 */
async function getAvailablePackagesFromApi(latitude, longitude, cacheKey, language = languages.EN) {
    let token = await authenticationService.getJWTFForUser();
    let clientToken = 'Bearer ' + token;
    let availability = await checkAvailableForLocation(clientToken, latitude, longitude);
    let packages = [];
    if (availability.status === PT1 || availability.status === PT2) {
        logger.info('add response to the cache');
        if (availability.status === PT1) {
            packages = await getPackages(availability.deviceId, availability.area, language);
        }
        if (cacheEnabled) {
            cache.setex(cacheKey, invalidationTimeout, JSON.stringify(
                new AvailabilityResponse(true, availability.status.name, availability.status.availableIn,
                    'API', packages, availability.area, availability.sto, availability.locId, availability.system, availability.deviceId))
            );
        }
    }
    return new AvailabilityResponse(true, availability.status.name, availability.status.availableIn,
        'API', packages, availability.area, availability.sto, availability.locId, availability.system, availability.deviceId);
}

function getNearRangeRequest(latitude, longitude, authorization) {
    const reqBody = {
        latitude,
        longitude,
        radius: '1',
        objectType: 'ALL'
    };
    return {
        method: 'POST',
        uri: inRangeApiHost,
        headers: {
            'Content-Type': 'application/json',
            Authorization: authorization,
        },
        json: reqBody,
        strictSSL: false
    };
}

async function checkNearRangeAvailability(latitude, longitude) {
    const authorization = `Bearer ${await authenticationService.getJWTFForUser()}`;
    const options = getNearRangeRequest(latitude, longitude, authorization);

    logger.info('requesting data from the nearby range API on: ', new Date());
    const result = await rp.post(options);
    logger.info('got data from the nearby range API on: ', new Date());
    logger.trace(`got data from the nearby range API on: ${new Date()} for ${JSON.stringify(options)} as ${JSON.stringify(result)}`);

    return (result['statusCode'] === '0' && result['data'] && result['data']['device'] &&
        result['data']['device'].length > 0 && result['data']['device'].some(d => d['object'] && d['object']['status'] === 'PLANNED')
    );
}

function getCheckCableNearRangeRequest(latitude, longitude, bearerToken) {
    const reqBody = {
        latitude,
        longitude: longitude,
        radius: '1',
        tolerance: '0',
        networkRole: 'ALL'
    };
    return {
        method: 'POST',
        uri: nearByCableHostApiHost,
        headers: {
            'Content-Type': 'application/json',
            Authorization: bearerToken,
        },
        json: reqBody,
        strictSSL: false
    };
}

async function checkCableNearRangeAvailability(latitude, longitude) {

    const token = 'Bearer ' + await authenticationService.getJWTFForUser();
    const options = getCheckCableNearRangeRequest(latitude, longitude, token);

    logger.info(`requesting data from the cable near API on: ${new Date()} as ${JSON.stringify(options)}`);
    const result = await rp.post(options);
    logger.info(`got data from the nearby cable near API on: ${new Date()} as ${JSON.stringify(result)}`);
    logger.trace(`got data from the cable near API on: ${new Date()} for ${JSON.stringify(options)} as ${JSON.stringify(result)}`);

    return (result['statusCode'] === '0' && result['data'] && result['data']['cableSheath']
        && result['data']['cableSheath'].length > 0);
}

//fixme:    how is this happening
function getAreaMapping(sto) {
    return 'DCS - UCS II';
}


function getReqBodyForCheckAvailabilityForLocation(latitude, longitude) {
    return {
        FeasibilityDevice: {
            customerID: '',
            services:
                {
                    service: [{
                        id: '',
                        name: 'INTERNET',
                        attribute: [
                            {
                                name: 'DOWNLOADSPEED',
                                value: ['51200']
                            },
                            {
                                name: 'UPLOADSPEED',
                                value: ['10240']
                            }
                        ]
                    }]
                },
            number: '',
            address: {
                latitude,
                longitude
            }
        }
    };
}

async function checkAvailableForLocation(authorization, latitude, longitude) {
    const reqBody = getReqBodyForCheckAvailabilityForLocation(latitude, longitude);

    const options = {
        method: 'POST',
        uri: nearByAvailabilityApiHost,
        headers: {
            'Content-Type': 'application/json',
            Authorization: authorization,
        },
        json: reqBody,
        strictSSL: false
    };

    logger.info(`requesting data from the API on: ${new Date()} as ${JSON.stringify(options)}`);
    const result = await rp.post(options);
    logger.info(`got data from the API on: ${new Date()} status ${JSON.stringify(result.statusCode)}`);
    logger.trace(`got data from the API on: ${new Date()} for ${JSON.stringify(options)} as ${JSON.stringify(result)}`);


    if (result['FeasibilityDeviceNearbyResponse'] && result['FeasibilityDeviceNearbyResponse']['FeasibilityDeviceResponse'] &&
        result['FeasibilityDeviceNearbyResponse']['FeasibilityDeviceResponse']['STO'] !== '') {

        const sto = result['FeasibilityDeviceNearbyResponse']['FeasibilityDeviceResponse']['STO'];
        const deviceId = result['FeasibilityDeviceNearbyResponse']['FeasibilityDeviceResponse']['DeviceID'];
        const locId = result['FeasibilityDeviceNearbyResponse']['FeasibilityDeviceResponse']['LOC_ID'];
        const system = result['FeasibilityDeviceNearbyResponse']['FeasibilityDeviceResponse']['System'];

        let area = getAreaMapping(sto);

        return {
            status: PT1,
            deviceId,
            area,
            sto,
            locId,
            system
        };
    }
    // check on nearby range : for PT2
    const pt2Response = {
        status: PT2,
        deviceId: null,
        area: null,
        sto: null,
        locId: null,
        system: null
    };
    if (await checkNearRangeAvailability(latitude, longitude)) {
        return pt2Response;
    }
    if (await checkCableNearRangeAvailability(latitude, longitude)) {
        return pt2Response;
    }
    // if nothing found, then PT3

    return {
        status: PT3,
        deviceId: null,
        area: null,
        sto: null,
        locId: null,
        system: null
    };


}

function getReqBodyForGetAvailablePackageInfo(deviceId, area) {
    return {
        guid: '3',
        code: '0',
        data: {
            'dev.Id': deviceId,
            area,
            devName: '',
            speed: '0',
            socio: 'Lain-lain',
            source: 'MI_APPS'
        }
    };
}

/**
 * @deprecated
 * @param packageXML
 * @returns {Package}
 */
function mapToPackage(packageXML) {
    let id = packageXML['PACKAGE_ID'];
    let clientId = packageXML['PACKAGE_ID'];
    let name = packageXML['FLAG'];
    let group = 'PLATINUM';
    let price = packageXML['PRICE_TOTAL'];
    let discountedPrice = packageXML['PRICE_TOTAL'];
    let description = new Description(packageXML['PACKAGE_DETAILS'], packageXML['PACKAGE_DETAILS'], '');
    let termsAndConditions = [new TermsAndConditions('', '', '')];
    let images = [''];
    let addons = [new Addon()];

    let speed = packageXML['SPEED'];
    let numberOfUsers = 0;
    let entertainment = [''];
    let internetAddons = [new InternetAddon()];
    let internetPackage = new InternetPackage(id, clientId, name, group, price, discountedPrice, description,
        termsAndConditions, images, speed, numberOfUsers, entertainment, internetAddons);
    let callPackage = new CallPackage();
    let tvPackage = new TVPackage();

    return new Package(id, clientId, name, group, price, discountedPrice, description, termsAndConditions,
        images, addons, internetPackage, callPackage, tvPackage);
}

async function findAvailablePackageInfoFromAPI(deviceId, area) {

    let token = await authenticationService.getJWTFForUser();
    const authorization = 'Bearer ' + token;

    const reqBody = getReqBodyForGetAvailablePackageInfo(deviceId, area);

    const options = {
        method: 'POST',
        uri: listPackagesHost,
        headers: {
            'Content-Type': 'application/json',
            Authorization: authorization,
        },
        json: reqBody,
        strictSSL: false
    };

    logger.info('requesting data from the API on: ', new Date());
    const result = await rp.post(options).catch(e => logger.error(`Error in getting package API: request: ${options} `, e));
    logger.info('got data from the API on: ', new Date());
    logger.trace(`got data from the API on: ${new Date()} for ${JSON.stringify(options)} as ${JSON.stringify(result)}`);
    if (result.statusCode === '0' && result.data) {
        return [].concat(...result.data
            .filter(d => d['packageXml'])
            .filter(d => d['packageXml'].length)
            .map(d => d['packageXml'])
        );
    }
    return [];

}

async function getPackages(deviceId, area, language) {
    let availableInternetPackages = await findAvailablePackageInfoFromAPI(deviceId, area);
    if (availableInternetPackages) {
        const mappedPackages = await packageService.filterPackageFromIds(availableInternetPackages.map(p => p.PACKAGE_ID), language);
        if (mappedPackages && mappedPackages.data) {
            return mappedPackages.data;
        }
    }
    return [];

}

module.exports.closeCache = (callback) => {
    if (cacheEnabled && cache) {
        logger.info('closing the caching connection');
        cache.quit(callback);
    }
};
