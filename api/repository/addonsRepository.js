/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Dulaj Pathirana on 26/11/2019, 9:55 AM
 */
const elastic = require('./db/elasticsearch/db');
const config = require('../config/config.js');

const {addonOutputProperties} = require('../models/products');

class AddonsRepository {

    constructor() {
        this.elastic = elastic;
        this.esConig = config.get('/elasticSearch');
        this.addonsIndex = config.get('/elasticSearchIndices').addons.index;
        this.addonsType = config.get('/elasticSearchIndices').addons.type;
        this.addonslimit = config.get('/elasticSearchIndices').addons.limit;
    }

    async getAllAddons(size = this.addonslimit, language = '') {
        const query = {
            index: `${this.addonsIndex}-${language}`,
            type: this.addonsType,
            size,
            body: {
                query: {
                    match_all: {}
                },
                sort: [
                    {
                        priority: {
                            order: 'asc'
                        }
                    },
                    {
                        'name.keyword': {
                            order: 'asc'
                        }
                    }
                ],
                _source: [...addonOutputProperties]
            }
        };
        return this.elastic.findAll(this.esConig, query);
    }

    async getAddonById(id, language = '') {
        const query = {
            index: `${this.addonsIndex}-${language}`,
            type: this.addonsType,
            body: {
                query: {
                    match: {id}
                },
                _source: [...addonOutputProperties]
            }
        };
        return this.elastic.findOne(this.esConig, query);
    }

    async addAddon(addon, language = '') {
        const data = {
            index: `${this.addonsIndex}-${language}`,
            type: this.addonsType,
            id: addon.id,
            body: addon
        };
        return this.elastic.insertData(this.esConig, data);
    }

    async updateAddon(addon, id, language = '') {
        const data = {
            index: `${this.addonsIndex}-${language}`,
            type: this.addonsType,
            id,
            body: {
                doc: addon
            }
        };
        return this.elastic.updateData(this.esConig, data);
    }

    async deleteAddon(id, language = '') {
        const query = {
            index: `${this.addonsIndex}-${language}`,
            type: this.addonsType,
            body: {
                query: {
                    match: {id}
                }
            }
        };
        return this.elastic.deleteData(this.esConig, query);
    }

    async findMatchedAddon(match, size = this.addonslimit, language = '') {
        const query = {
            index: `${this.addonsIndex}-${language}`,
            type: this.addonsType,
            size,
            body: {
                query: match,
                _source: [...addonOutputProperties]
            }
        };
        return this.elastic.findAll(this.esConig, query);
    }

}

module.exports = AddonsRepository;
