/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Dulaj Pathirana on 20/11/2019, 12:03 PM
 */
const elastic = require('./db/elasticsearch/db');
const config = require('../config/config.js');
const {packageOutputProperties} = require('../models/products');

class PackageRepository {

    constructor() {
        this.elastic = elastic;
        this.esConig = config.get('/elasticSearch');
        this.packagesIndex = config.get('/elasticSearchIndices').packages.index;
        this.packagesType = config.get('/elasticSearchIndices').packages.type;
        this.packageslimit = config.get('/elasticSearchIndices').packages.limit;

    }

    async getAllPackages(language, size = this.packageslimit) {
        const query = {
            index: `${this.packagesIndex}-${language}`,
            type: this.packagesType,
            size,
            body: {
                query: {
                    match_all: {}
                }
            }
        };
        return this.elastic.findAll(this.esConig, query);
    }

    async getPackageById(id, language) {
        const query = {
            index: `${this.packagesIndex}-${language}`,
            type: this.packagesType,
            body: {
                query: {
                    match: {id}
                }
            }
        };
        return this.elastic.findOne(this.esConig, query);
    }

    async addPackage(pkg, language) {
        const data = {
            index: `${this.packagesIndex}-${language}`,
            type: this.packagesType,
            id: pkg.id,
            body: pkg
        };
        return this.elastic.insertData(this.esConig, data);
    }

    async updatePackage(pkg, id, language) {
        const data = {
            index: `${this.packagesIndex}-${language}`,
            type: this.packagesType,
            id,
            body: {
                doc: pkg
            }
        };
        return this.elastic.updateData(this.esConig, data);
    }

    async deletePackage(id, language) {
        const query = {
            index: `${this.packagesIndex}-${language}`,
            type: this.packagesType,
            body: {
                query: {
                    match: {id}
                }
            }
        };
        return this.elastic.deleteData(this.esConig, query);
    }

    async findMatchedProduct(match, language, size = this.packageslimit, filter = true) {
        const query = {
            index: `${this.packagesIndex}-${language}`,
            type: this.packagesType,
            size,
            body: {
                query: match,
                _source: {
                    includes: packageOutputProperties
                }
            }
        };
        return this.elastic.findAll(this.esConig, query);
    }

}

module.exports = PackageRepository;
