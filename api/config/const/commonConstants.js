/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Dulaj Pathirana on 8/1/2020, 12:41 PM
 */


module.exports = {
    languages: {
        EN: 'en',
        ID: 'id'
    },
    languageHeader: 'accept-language'
};
