/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Dulaj Pathirana on 26/9/2019, 11:30 AM
 */

module.exports = {
    PT1: {
        name: 'PT1',
        availableIn: '0 days',
        message: 'IndiHome services is available in your area!'
    },
    PT2: {
        name: 'PT2',
        availableIn: '1-3 months',
        message:  'Sorry, IndiHome is not yet available in your area!'
    },
    PT3: {
        name: 'PT3',
        availableIn: 'not available',
        message: 'Sorry, IndiHome is not yet available in your area!'
    }
};
