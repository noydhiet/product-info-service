/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Dulaj Pathirana on 12/9/2019, 12:19 PM
 */
require('dotenv').config();
const confidence = require('confidence');

const config = {
    availability: {
        host: process.env.AVAILABILITY_API_HOST,
        nearByHost: process.env.AVAILABILITY_NEARBY_API_HOST,
        inRangeHost: process.env.AVAILABILITY_IN_RANGE_API_HOST,
        inNearByCableHost: process.env.AVAILABILITY_CABLE_IN_RANGE_API_HOST,
        nearByRange: process.env.AVAILABILITY_RANGE,
        listPackagesHost: process.env.AVAILABILITY_LIST_PACKAGES_API_HOST
    },
    authorization: {
        host: process.env.AUTHORIZATION_API_HOST,
        endpoint: process.env.AUTHORIZATION_API_ENDPOINT,
        token: process.env.AUTHORIZATION_TOKEN,
        timeOut: process.env.AUTHORIZATION_TIMEOUT
    },
    authentication: {
        token: process.env.SHARED_SECRET
    },
    cache: {
        isEnabled: process.env.CACHE_ENABLE,
        host: process.env.REDIS_CACHE_HOST,
        port: process.env.REDIS_CACHE_PORT,
        invalidationTimeout: process.env.CACHE_INVALIDATION_TIMEOUT
    },
    basicAuthApi: [
        {
            username: process.env.BASIC_AUTH_USERNAME,
            password: process.env.BASIC_AUTH_PASSWORD
        }
    ],
    elasticSearch: {
        connectionClass: process.env.ELASTICSEARCH_CONNECTION_CLASS,
        apiVersion: process.env.ELASTICSEARCH_API_VERSION,
        host: [
            process.env.ELASTICSEARCH_HOST
        ],
        maxRetries: process.env.ELASTICSEARCH_MAX_RETRIES,
        requestTimeout: process.env.ELASTICSEARCH_REQUEST_TIMEOUT
    },
    elasticSearchIndices: {
        packages: {
            index: 'packages',
            type: '_doc',
            limit: 1000
        },
        addons: {
            index: 'addons',
            type: '_doc',
            limit: 1000
        },
        promotions: {
            index: 'promotions',
            type: '_doc',
            limit: 1000
        }
    },
    packagesInfo: {
        priceFilter: {
            retries: 5,
            upperBoundRatio: 1.2,
        }
    },
    profileService: {
        host: process.env.PROFILE_SERVICE_API_HOST,
        locationUpdateEndpoint: process.env.PROFILE_SERVICE_LOCAION_UPDATE_ENDPOINT
    },
    logLevel: process.env.LOG_LEVEL,
    requestLogType: process.env.REQEST_LOG_TYPE
};

const store = new confidence.Store(config);

exports.get = (key) => store.get(key);
