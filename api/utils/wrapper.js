const {
    NotFoundError, InternalServerError, BadRequestError, ConflictError, ConditionNotMetError, PreconditionFailedError,
    ForbiddenError, MethodNotAllowedError, UnauthorizedError, ServiceUnavailableError,
} = require('./error');

const {ERROR: httpError} = require('./httpStatus/statusCode');

const data = d => ({error: null, data: d});

const paginationData = (d, meta) => ({error: null, data: d, meta});

const error = error => ({error, data: null});

const response = (res, type, result, message = '', statusCode = 200, code = statusCode) => {
    let ok = true;
    let {data: d} = result;
    if (type === 'fail') {
        ok = false;
        d = {};
        message = result.error.message || message;
        statusCode = checkErrorCode(result.error);
    }
    if (statusCode !== 200) {
        code = statusCode;
    }
    res.status(statusCode).send({
        ok,
        data: d,
        message,
        status: code,
    });
};

/**
 *
 * @param {Object}  res
 * @param {Boolean} ok  based on success: true, fail: false
 * @param {Object}  result  object with data and error fields, even in the error, can pass the data field is available
 * @param {String}  message
 * @param {Number}  httpStatusCode    HTTP status code
 * @param {Number}  status    response body's status code (Custom Status Code)
 */
const formattedResponse = (res, ok, result, message = '', httpStatusCode = 200, status = null) => {

    if (ok) {
        return res.status(httpStatusCode).send({
            ok,
            data: result.data || {},
            message,
            status: status || httpStatusCode
        });
    }

    return res.status(httpStatusCode).send({
        ok,
        data: result.data || result.error.data || {},
        message: message || result.error.message || '',
        status: status || result.error.code || checkErrorCode(result.error || new Error())
    });

};

const paginationResponse = (res, type, result, message = '', code = 200) => {
    let ok = true;
    let {data: d} = result;
    if (type === 'fail') {
        ok = false;
        d = {};
        message = result.error.message || message;
        code = checkErrorCode(result.error);
    }
    res.status(code).send({
        ok,
        data: d,
        meta: result.meta,
        status: code,
        message,
    });
};

const checkErrorCode = (error) => {
    switch (error.constructor) {
    case BadRequestError:
        return httpError.BAD_REQUEST;
    case ConflictError:
        return httpError.CONFLICT;
    case ConditionNotMetError:
        return httpError.CONDITION_NOT_MET;
    case ForbiddenError:
        return httpError.FORBIDDEN;
    case InternalServerError:
        return httpError.INTERNAL_ERROR;
    case NotFoundError:
        return httpError.NOT_FOUND;
    case PreconditionFailedError:
        return httpError.PRECONDITION_FAILED;
    case MethodNotAllowedError:
        return httpError.METHOD_NOT_ALLOWED;
    case UnauthorizedError:
        return httpError.UNAUTHORIZED;
    case ServiceUnavailableError:
        return httpError.SERVICE_UNAVAILABLE;
    default:
        return httpError.CONFLICT;
    }
};

module.exports = {
    data,
    paginationData,
    error,
    response,
    formattedResponse,
    paginationResponse,
    checkErrorCode
};
