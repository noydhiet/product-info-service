
const CommonError = require('./commonError');
const {ERROR} = require('../httpStatus/statusCode');

class MethodNotAllowed extends CommonError {
    constructor(message) {
        super(message || 'Method Not Allowed');
        this.code = ERROR.METHOD_NOT_ALLOWED;
    }
}

module.exports = MethodNotAllowed;
