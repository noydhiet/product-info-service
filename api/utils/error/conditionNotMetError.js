const CommonError = require('./commonError');
const {ERROR} = require('../httpStatus/statusCode');

class ConditionNotMetError extends CommonError {
    constructor(message) {
        super(message || 'Condition Not Met Failed');
        this.code = ERROR.CONDITION_NOT_MET;
    }
}

module.exports = ConditionNotMetError;
