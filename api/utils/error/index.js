const ConflictError = require('./conflictError');
const ConditionNotMetError = require('./conditionNotMetError');
const ForbiddenError = require('./forbiddenError');
const InternalServerError = require('./internalServerError');
const NotFoundError = require('./notFoundError');
const UnauthorizedError = require('./unauthorizedError');
const PreconditionFailedError = require('./preconditionFailedError');
const MethodNotAllowedError = require('./methodNotAllowed');
const BadRequestError = require('./badRequestError');
const ServiceUnavailableError = require('./serviceUnavaiableError');

module.exports = {
    ConflictError,
    ConditionNotMetError,
    ForbiddenError,
    InternalServerError,
    NotFoundError,
    UnauthorizedError,
    PreconditionFailedError,
    MethodNotAllowedError,
    BadRequestError,
    ServiceUnavailableError,
};
