const {ERROR} = require('../httpStatus/statusCode');

class CommonError extends Error {
    constructor(message) {
        super(message);
        this.code = ERROR.CONFLICT;
    }

}

module.exports = CommonError;
