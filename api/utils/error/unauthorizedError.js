
const CommonError = require('./commonError');
const {ERROR} = require('../httpStatus/statusCode');

class Unauthorized extends CommonError {
    constructor(message) {
        super(message || 'Unauthorized');
        this.code = ERROR.UNAUTHORIZED;
    }
}

module.exports = Unauthorized;
