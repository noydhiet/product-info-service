
const CommonError = require('./commonError');
const {ERROR} = require('../httpStatus/statusCode');

class BadRequestError extends CommonError {
    constructor(message) {
        super(message || 'Bad Request');
        this.code = ERROR.BAD_REQUEST;
    }
}

module.exports = BadRequestError;
