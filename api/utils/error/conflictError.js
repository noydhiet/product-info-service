
const CommonError = require('./commonError');
const {ERROR} = require('../httpStatus/statusCode');

class ConflictError extends CommonError {
    constructor(message) {
        super(message || 'Conflict');
        this.code = ERROR.CONFLICT;
    }
}

module.exports = ConflictError;
