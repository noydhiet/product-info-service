const CommonError = require('./commonError');
const {ERROR} = require('../httpStatus/statusCode');

class PreconditionFailedError extends CommonError {
    constructor(message, data = {}) {
        super(message || 'Precondition Failed');
        this.data = data;
        this.code = ERROR.PRECONDITION_FAILED;
    }
}

module.exports = PreconditionFailedError;
