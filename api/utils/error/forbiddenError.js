
const CommonError = require('./commonError');
const {ERROR} = require('../httpStatus/statusCode');

class ForbiddenError extends CommonError {
    constructor(message, data = {}) {
        super(message || 'Forbidden');
        this.data = data;
        this.code = ERROR.FORBIDDEN;
    }
}

module.exports = ForbiddenError;
