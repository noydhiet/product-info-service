
const CommonError = require('./commonError');
const {ERROR} = require('../httpStatus/statusCode');

class InternalServerError extends CommonError {
    constructor(message) {
        super(message || 'Internal Server Error');
        this.code = ERROR.INTERNAL_ERROR;
    }
}

module.exports = InternalServerError;
