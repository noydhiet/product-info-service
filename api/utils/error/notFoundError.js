
const CommonError = require('./commonError');
const {ERROR} = require('../httpStatus/statusCode');

class NotFoundError extends CommonError {
    constructor(message) {
        super(message || 'Not Found');
        this.code = ERROR.NOT_FOUND;
    }
}

module.exports = NotFoundError;
