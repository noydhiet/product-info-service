/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Dulaj Pathirana on 12/9/2019, 12:19 PM
 */

const ERROR = {
    CONDITION_NOT_MET: 304,
    BAD_REQUEST: 400,
    UNAUTHORIZED: 401,
    FORBIDDEN: 403,
    NOT_FOUND: 404,
    METHOD_NOT_ALLOWED: 405,
    CONFLICT: 409,
    PRECONDITION_FAILED: 412,
    INTERNAL_ERROR: 500,
    SERVICE_UNAVAILABLE: 503,
};

const SUCCESS = {
    OK: 200,
    CREATED: 201,
    ACCEPTED: 202,
};

module.exports = {
    ERROR,
    SUCCESS,
};
