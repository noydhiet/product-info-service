/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Alif Septian
 */

const {BadRequestError} = require('./error');
const wrapper = require('./wrapper');

const isValidPayload = (payload, constraint) => {
    const {value, error} = constraint.validate(payload);
    if (error) {
        const message = error.details.shift().message.replace(/"/g, '');
        return wrapper.error(new BadRequestError(message));
    }
    return wrapper.data(value);

};

module.exports = {
    isValidPayload
};
