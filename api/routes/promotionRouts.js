/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Dulaj Pathirana on 18/12/2019, 4:16 PM
 */

const express = require('express');
const router = express.Router();

const userAuth = require('./middlewares/userAuth');

const promotionController = require('./controllers/promotionController');

router.get('/', userAuth, promotionController.getPromotions);
router.get('/:id', userAuth, promotionController.getPromotionByID);

module.exports = router;
