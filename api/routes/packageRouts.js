/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Dulaj Pathirana on 18/12/2019, 4:16 PM
 */

const express = require('express');
const router = express.Router();

const userAuth = require('./middlewares/userAuth');
const basicAuth = require('./middlewares/basicAuth');
const commonAuth = require('./middlewares/commonAuth');

const packageController = require('./controllers/packageController');

//  Availability
router.get('/availability', userAuth, packageController.getPackagesAvailability);
router.post('/installation', userAuth, packageController.getAvailabilityAndSaveLocation);
router.delete('/availability/cache', basicAuth.isAuthenticated, packageController.clearCache);

//  Packages
router.get('/', commonAuth, packageController.getPackages);
router.post('/', basicAuth.isAuthenticated, packageController.addPackage);
router.put('/:id', userAuth, packageController.updatePackage);
router.get('/:id', commonAuth, packageController.getPackagesById);
router.delete('/:id', userAuth, packageController.deletePackageById);

//  Search: for the smart package finder
router.get('/price/search/', userAuth, packageController.packagesPriceSearch);
router.get('/needs/search/', userAuth, packageController.packagesNeedsSearch);

module.exports = router;
