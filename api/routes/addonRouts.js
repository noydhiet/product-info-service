/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Dulaj Pathirana on 18/12/2019, 4:16 PM
 */

const express = require('express');
const router = express.Router();

const basicAuth = require('./middlewares/basicAuth');
const userAuth = require('./middlewares/userAuth');

const addonController = require('./controllers/addonController');


router.get('/', userAuth, addonController.getAddons);
router.post('/', basicAuth.isAuthenticated, addonController.addAddon);
router.put('/:id', userAuth, addonController.updateAddon);
router.get('/:id', userAuth, addonController.getAddonById);
router.delete('/:id', userAuth, addonController.deleteAddonById);

module.exports = router;
