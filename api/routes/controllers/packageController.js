/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Dulaj Pathirana on 12/9/2019, 12:19 PM
 */

const availabilityService = require('../../services/packageAvailabilityService');
const packagesService = require('../../services/packagesService');
const responseWrapper = require('../../utils/wrapper');
const validator = require('validator');
const {BadRequestError} = require('../../utils/error');

const Logger = require('../../utils/logger');
const {languageHeader} = require("../../config/const/commonConstants");
const logger = new Logger('PackageController');

module.exports.getPackagesAvailability = (req, res) => {
    let latitude = req.query.latitude;
    let longitude = req.query.longitude;
    logger.info(`user: ${req.userData.userId}, checking for new installation in : ${latitude} , ${longitude}`);

    if (validator.isLatLong(latitude + ', ' + longitude)) {
        availabilityService.getPackagesAvailability(latitude, longitude, true)
            .then(data => {
                return responseWrapper.formattedResponse(res, true, {data});
            })
            .catch(error => {
                return responseWrapper.formattedResponse(res, false, {error});
            });
    } else {
        return responseWrapper.formattedResponse(res, false, responseWrapper.error(new BadRequestError('invalid latitude or longitude')));
    }
};

module.exports.getAvailabilityAndSaveLocation = (req, res) => {
    const latitude = req.body.latitude;
    const longitude = req.body.longitude;

    logger.info(`user: ${req.userData.userId}, checking for new installation in ${JSON.stringify(req.body)}`);

    if (validator.isLatLong(latitude + ', ' + longitude)) {
        //todo: if device ID is unique, will have to disable the cache
        availabilityService.getPackagesAvailabilityAndSaveLocation(latitude, longitude, false, req.body, req.userData)
            .then(data => {
                return responseWrapper.formattedResponse(res, true, {data});
            })
            .catch(error => {
                return responseWrapper.formattedResponse(res, false, {error});
            });
    } else {
        return responseWrapper.formattedResponse(res, false, responseWrapper.error(new BadRequestError('invalid latitude or longitude')));
    }
};

module.exports.clearCache = (req, res) => {
    availabilityService.clearCache().then(data => {
        responseWrapper.formattedResponse(res, true, {data}, 'cache cleared');
    }).catch(err => {
        responseWrapper.formattedResponse(res, false, responseWrapper.error(err));
    });
};

module.exports.getPackages = (req, res) => {

    const language = req.headers[languageHeader];
    packagesService.getPackages(language)
        .then(response => {
            if (response.error) {
                return responseWrapper.formattedResponse(res, false, {error: response.error});
            }
            return responseWrapper.formattedResponse(res, true, {data: response.data}, 'packages found');
        })
        .catch(reason => {
            return responseWrapper.formattedResponse(res, false, {error: reason});
        });
};

module.exports.getPackagesById = (req, res) => {
    let id = req.params.id;
    const language = req.headers[languageHeader];
    packagesService.getPackagesById(id, language)
        .then(data => {
            return responseWrapper.formattedResponse(res, true, data, 'package found');
        })
        .catch(reason => {
            return responseWrapper.formattedResponse(res, false, {error: reason});
        });
};

module.exports.addPackage = (req, res) => {
    const pkg = req.body;
    const language = req.headers[languageHeader];
    packagesService.addPackage(pkg, language)
        .then(data => {
            if (data.error) {
                return responseWrapper.formattedResponse(res, false, responseWrapper.error(data.error));
            }
            return responseWrapper.formattedResponse(res, true, data.data, 'package added', 201);
        })
        .catch(reason => {
            return responseWrapper.formattedResponse(res, false, {error: reason});
        });
};

module.exports.updatePackage = (req, res) => {
    let pkg = req.body;
    let id = req.params.id;

    const language = req.headers[languageHeader];
    packagesService.updatePackage(pkg, id, language)
        .then(response => {
            if (response.error) {
                return responseWrapper.formattedResponse(res, false, response.error);
            }
            return responseWrapper.formattedResponse(res, true, response, 'packages updated');
        })
        .catch(reason => {
            return responseWrapper.formattedResponse(res, false, {error: reason});
        });
};


module.exports.deletePackageById = (req, res) => {
    let id = req.params.id;
    const language = req.headers[languageHeader];
    packagesService.deletePackageById(id, language)
        .then(response => {
            if (response.error) {
                return responseWrapper.formattedResponse(res, false, response.error);
            }
            return responseWrapper.formattedResponse(res, true, responseWrapper.data(response.data), 'packages deleted');
        })
        .catch(reason => {
            return responseWrapper.formattedResponse(res, false, {error: reason});
        });
};


module.exports.packagesNeedsSearch = (req, res) => {
    let needs = req.query.needs;
    const language = req.headers[languageHeader];
    packagesService.packagesNeedsSearch(needs, language)
        .then(response => {
            if (response.error) {
                return responseWrapper.formattedResponse(res, false, response.error);
            }
            return responseWrapper.formattedResponse(res, true, responseWrapper.data(response.data), 'packages found');
        })
        .catch(reason => {
            return responseWrapper.formattedResponse(res, false, {error: reason});
        });
};

module.exports.packagesPriceSearch = (req, res) => {
    let price = req.query.price;
    const language = req.headers[languageHeader];
    packagesService.packagesPriceSearch(price, language)
        .then(response => {
            if (response.error) {
                return responseWrapper.formattedResponse(res, false, response.error);
            }
            return responseWrapper.formattedResponse(res, true, responseWrapper.data(response.data), 'packages found');
        })
        .catch(reason => {
            return responseWrapper.formattedResponse(res, false, {error: reason});
        });
};
