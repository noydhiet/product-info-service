/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Dulaj Pathirana on 26/11/2019, 10:02 AM
 */

const addonService = require('../../services/addonsService');
const wrapper = require('../../utils/wrapper');

module.exports.getAddons = (req, res) => {
    const limit = req.query.limit;
    addonService.getAddons(limit)
        .then(response => {
            if (response.error) {
                wrapper.formattedResponse(res, false, response.error);
            }
            wrapper.formattedResponse(res, true, wrapper.data(response.data), 'addons found');
        })
        .catch(reason => {
            wrapper.formattedResponse(res, false, wrapper.error(reason));
        });
};

module.exports.getAddonById = (req, res) => {
    let id = req.params.id;
    addonService.getAddonById(id)
        .then(response => {
            wrapper.formattedResponse(res, true, wrapper.data(response), 'addon found');
        })
        .catch(reason => {
            wrapper.formattedResponse(res, false, wrapper.error(reason));
        });
};

module.exports.addAddon = (req, res) => {
    let pkg = req.body;
    addonService.addAddon(pkg)
        .then(response => {
            if (response.error) {
                return wrapper.formattedResponse(res, false, wrapper.error(response.error));
            }
            return wrapper.formattedResponse(res, true, wrapper.data(response.data), 'addon added', 201);
        })
        .catch(reason => {
            return wrapper.formattedResponse(res, false, wrapper.error(reason));
        });
};

module.exports.updateAddon = (req, res) => {
    let pkg = req.body;
    let id = req.params.id;

    addonService.updateAddon(pkg, id)
        .then(response => {
            if (response.error) {
                wrapper.formattedResponse(res, false, response.error);
            }
            wrapper.formattedResponse(res, true, wrapper.data(response.data), 'addons updated');
        })
        .catch(reason => {
            wrapper.formattedResponse(res, false, wrapper.error(reason));
        });
};


module.exports.deleteAddonById = (req, res) => {
    let id = req.params.id;

    addonService.deleteAddonById(id)
        .then(response => {
            if (response.error) {
                wrapper.formattedResponse(res, false, response.error);
            }
            wrapper.formattedResponse(res, true, wrapper.data(response.data), 'addons deleted');
        })
        .catch(reason => {
            wrapper.formattedResponse(res, false, wrapper.error(reason));
        });
};
