/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Dulaj Pathirana on 21/10/2019, 5:09 PM
 */

const wrapper = require('../../utils/wrapper');
const {getPromotionByID, getPromotions} = require('../../services/promotionsService');

//fixme: implement these, and remove the mock
module.exports.getPromotionByID = (req, res) => {
    let id = req.params.id;
    getPromotionByID(id)
        .then(data => {
            wrapper.formattedResponse(res, true, {data});
        })
        .catch(error => {
            wrapper.formattedResponse(res, false, {error});
        });
};

module.exports.getPromotions = (req, res) => {
    getPromotions()
        .then((data) => {
            wrapper.formattedResponse(res, true, {data});
        })
        .catch(error => {
            wrapper.formattedResponse(res, false, {error});
        });
};
