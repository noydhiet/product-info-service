/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Dulaj Pathirana on 17/9/2019, 2:44 PM
 */

const jwt = require('jsonwebtoken');
const config = require('../../config/config');
const wrapper = require('../../utils/wrapper');
const {UnauthorizedError} = require('../../utils/error');

const basicAuth = require('./basicAuth');

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
module.exports = (req, res, next) => {

    try {
        const token = req.headers.authorization.split(' ')[1];
        if (req.headers.authorization.split(' ')[0] === 'Bearer') {
            req.userData = jwt.verify(token, config.get('/authentication').token);
            next();
        } else {
            return basicAuth.isAuthenticated(req, res, next);
        }
    } catch (error) {
        return wrapper.formattedResponse(res, false, wrapper.error(new UnauthorizedError('Auth failed')), '', 401, 401);
    }
};
