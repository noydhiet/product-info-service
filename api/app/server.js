/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Dulaj Pathirana on 21/10/2019, 10:10 AM
 */

const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
const swaggerUi = require('swagger-ui-express');
const wrapper = require('../utils/wrapper');

const bodyParser = require('body-parser');

const fs = require('fs');
const path = require('path');
const spec = fs.readFileSync(path.join(__dirname, '../app/swagger.yaml'), 'utf8');

const jsyaml = require('js-yaml');
const swaggerDocument = jsyaml.safeLoad(spec);

const config = require('../config/config');
const reqLogType = config.get('/requestLogType');

const packageRouts = require('../routes/packageRouts');
const addonRouts = require('../routes/addonRouts');
const promotionsRouts = require('../routes/promotionRouts');

const basicAuth = require('../routes/middlewares/basicAuth');

function AppServer() {
    this.server = express();

    this.server.use(bodyParser.json());

    //todo: control cors from env config, current allow all
    this.server.use(cors());

    this.server.use(morgan(reqLogType));

    this.server.use(basicAuth.init());

    this.server.get('/', (req, res) => {
        wrapper.response(res, 'success', wrapper.data('Product info services'), 'This services is running properly.');
    });

    //swagger
    this.server.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

    //Packages
    this.server.use('/packages', packageRouts);

    //  Add-ons
    this.server.use('/addons', addonRouts);

    //  Promotions
    this.server.use('/promotions', promotionsRouts);

}

module.exports = AppServer;
