openapi: 3.0.0
info:
  description: "API Documentation for Products Info Service"
  version: "1.0.0"
  title: "Products Info REST API"
tags:
  - name: "Availability"
    description: "APIs for Check Availaiblity"
  - name: "Packages"
    description: "APIs for Package Information"
  - name: "Search"
    description: "APIs for Search and Filterization"
  - name: "Get by ID"
    description: "APIs for Get Product information By ID"

paths:
  '/packages/availability':
    get:
      tags:
        - "Availability"
        - "Packages"
      summary: "Find Package Availability for the User Location"
      operationId: "getPackagesAvailability"
      parameters:
        - name: "latitude"
          in: "query"
          description: "latitude of the user location"
          required: true
          schema:
            type: "string"
        - name: "longitude"
          in: "query"
          description: "longitude of the user location"
          required: true
          schema:
            type: "string"
      security:
        - BearerAuth: []
      responses:
        200:
          description: "OK"
          content:
            application/json:
              schema:
                properties:
                  status:
                    type: "integer"
                    example: 200
                  message:
                    type: "string"
                  ok:
                    type: "boolean"
                  data:
                    $ref: '#/components/schemas/AvailablePackagesResponse'
        401:
          description: "Unauthorized"
        404:
          description: "Failed. Data not found."
        409:
          description: "Conflict. API connection lost."
      x-swagger-router-controller: "packageController"

  '/packages/installation':
    post:
      tags:
        - "Availability"
        - "Packages"
      summary: "Find Package Availability for the Installation Location"
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/InstallationLocation'
      security:
        - BearerAuth: []
      responses:
        200:
          description: "OK"
          content:
            application/json:
              schema:
                properties:
                  status:
                    type: "integer"
                    example: 200
                  message:
                    type: "string"
                  ok:
                    type: "boolean"
                  data:
                    $ref: '#/components/schemas/AvailablePackagesResponse'
        401:
          description: "Unauthorized"
        404:
          description: "Failed. Data not found."
        409:
          description: "Conflict. API connection lost."
        503:
          description: "Service Not Available. ODP not available."

  '/packages/availability/cache':
    delete:
      tags:
        - "Availability"
        - "Packages"
      summary: "Find Package Availability for the User Location"
      operationId: "deletePackagesAvailabilityCache"
      security:
        - BasicAuth: []
      responses:
        200:
          description: "OK"
          content:
            application/json:
              schema:
                properties:
                  status:
                    type: "integer"
                    example: 200
                  message:
                    type: "string"
                  ok:
                    type: "boolean"
                  data:
                    type: "string"
        401:
          description: "Unauthorized"
        404:
          description: "Failed. Data not found."
        409:
          description: "Conflict. API connection lost."
      x-swagger-router-controller: "packageController"

  '/packages':
    get:
      tags:
        - "Packages"
      summary: "List all the packages"
      operationId: "getPackages"
      parameters:
        - name: Accept-Language
          in: header
          required: true
          description: App Language
          schema:
            type: string
            enum: ['en', 'id']
            default: 'en'
      security:
        - BearerAuth: []
      responses:
        200:
          description: "OK"
        401:
          description: "Unauthorized"
        404:
          description: "Failed. Data not found."
        409:
          description: "Conflict. API connection lost."
      x-swagger-router-controller: "packageController"
    post:
      requestBody:
        content:
          application/json:
            schema:
              type: object
      tags:
        - "Packages"
      parameters:
        - name: Accept-Language
          in: header
          required: true
          description: App Language
          schema:
            type: string
            enum: ['en', 'id']
            default: 'en'
      responses:
        201:
          description: "Created"
        401:
          description: "Unauthorized"
        404:
          description: "Failed. Data not found."
        409:
          description: "Conflict. API connection lost."
      security:
        - BasicAuth: []

  '/packages/{id}':
    get:
      tags:
        - "Get by ID"
        - "Packages"
      summary: "get package by ID"
      operationId: "getPackagesById"
      security:
        - BearerAuth: []
      parameters:
        - name: id
          in: path
          schema:
            type: string
          required: true
        - name: Accept-Language
          in: header
          required: true
          description: App Language
          schema:
            type: string
            enum: ['en', 'id']
            default: 'en'
      responses:
        200:
          description: "OK"
        401:
          description: "Unauthorized"
        404:
          description: "Failed. Data not found."
        409:
          description: "Conflict. API connection lost."
      x-swagger-router-controller: "packageController"
    put:
      parameters:
        - name: id
          in: path
          schema:
            type: string
          required: true
      requestBody:
        content:
          application/json:
            schema:
              type: object
      tags:
        - "Packages"
      responses:
        201:
          description: "Created"
        401:
          description: "Unauthorized"
        404:
          description: "Failed. Data not found."
        409:
          description: "Conflict. API connection lost."
      security:
        - BasicAuth: []
    delete:
      tags:
        - "Packages"
      summary: "delete package by ID"
      operationId: "deletePackagesById"
      security:
        - BasicAuth: []
      parameters:
        - name: id
          in: path
          schema:
            type: string
          required: true
      responses:
        200:
          description: "OK"
        401:
          description: "Unauthorized"
        404:
          description: "Failed. Data not found."
        409:
          description: "Conflict. API connection lost."
      x-swagger-router-controller: "packageController"

  '/packages/price/search/':
    get:
      tags:
        - "Packages"
        - "Search"
      summary: "filter all packages by price"
      operationId: "packagesPriceSearch"
      security:
        - BearerAuth: []
      parameters:
        - name: "price"
          in: "query"
          schema:
            type: "string"
        - name: Accept-Language
          in: header
          required: true
          description: App Language
          schema:
            type: string
            enum: ['en', 'id']
            default: 'en'
      responses:
        200:
          description: "OK"
        401:
          description: "Unauthorized"
        404:
          description: "Failed. Data not found."
        409:
          description: "Conflict. API connection lost."
      x-swagger-router-controller: "packageController"

  '/packages/needs/search/':
    get:
      tags:
        - "Packages"
        - "Search"
      summary: "filter all packages by needs"
      operationId: "packagesNeedsSearch"
      security:
        - BearerAuth: []
      parameters:
        - name: "needs"
          in: "query"
          schema:
            type: "array"
            items:
              type: "string"
      responses:
        200:
          description: "OK"
        401:
          description: "Unauthorized"
        404:
          description: "Failed. Data not found."
        409:
          description: "Conflict. API connection lost."
      x-swagger-router-controller: "packageController"

  '/addons':
    get:
      tags:
        - "Addons"
      summary: "List all the addons"
      operationId: "getAddons"
      security:
        - BearerAuth: []
      responses:
        200:
          description: "OK"
        401:
          description: "Unauthorized"
        404:
          description: "Failed. Data not found."
        409:
          description: "Conflict. API connection lost."
      x-swagger-router-controller: "packageController"
    post:
      requestBody:
        content:
          application/json:
            schema:
              type: object
      tags:
        - "Addons"
      responses:
        201:
          description: "Created"
        401:
          description: "Unauthorized"
        404:
          description: "Failed. Data not found."
        409:
          description: "Conflict. API connection lost."
      security:
        - BasicAuth: []

  '/addons/{id}':
    get:
      tags:
        - "Get by ID"
        - "Addons"
      summary: "get addon by ID"
      operationId: "getAddonsById"
      security:
        - BearerAuth: []
      parameters:
        - name: id
          in: path
          schema:
            type: string
          required: true
      responses:
        200:
          description: "OK"
        401:
          description: "Unauthorized"
        404:
          description: "Failed. Data not found."
        409:
          description: "Conflict. API connection lost."
      x-swagger-router-controller: "packageController"
    put:
      parameters:
        - name: id
          in: path
          schema:
            type: string
          required: true
      requestBody:
        content:
          application/json:
            schema:
              type: object
      tags:
        - "Packages"
      responses:
        201:
          description: "Created"
        401:
          description: "Unauthorized"
        404:
          description: "Failed. Data not found."
        409:
          description: "Conflict. API connection lost."
      security:
        - BasicAuth: []
    delete:
      tags:
        - "Addons"
      summary: "delete package by ID"
      operationId: "deleteAddonById"
      security:
        - BasicAuth: []
      parameters:
        - name: id
          in: path
          schema:
            type: string
          required: true
      responses:
        200:
          description: "OK"
        401:
          description: "Unauthorized"
        404:
          description: "Failed. Data not found."
        409:
          description: "Conflict. API connection lost."
      x-swagger-router-controller: "packageController"

components:
  schemas:

    Description:
      type: "object"
      properties:
        title:
          type: "string"
        subTitle:
          type: "string"
        image:
          type: "string"

    TermsAndConditions:
      type: "object"
      properties:
        title:
          type: "string"
        text:
          type: "string"

    AddOn:
      type: "object"
      properties:
        id:
          type: "string"
        cilentId:
          type: "string"
        name:
          type: "string"
        description:
          $ref: '#/components/schemas/Description'
        price:
          type: "number"
        discontedPrice:
          type: "number"
        images:
          type: "array"
          items:
            type: "string"
        termsAndConditions:
          type: "array"
          items:
            $ref: '#/components/schemas/TermsAndConditions'

    InternetPackage:
      type: "object"
      properties:
        id:
          type: "string"
        cilentId:
          type: "string"
        name:
          type: "string"
        description:
          $ref: '#/components/schemas/Description'
        price:
          type: "number"
        discontedPrice:
          type: "number"
        speed:
          type: "number"
        minNumberOfPeople:
          type: "integer"
        maxNumberOfPeople:
          type: "integer"
        usageTypes:
          type: "array"
          items:
            type: "string"
        images:
          type: "array"
          items:
            type: "string"
        addons:
          type: "array"
          items:
            $ref: '#/components/schemas/AddOn'
        termsAndConditions:
          type: "array"
          items:
            $ref: '#/components/schemas/TermsAndConditions'

    TVPackage:
      type: "object"
      properties:
        id:
          type: "string"
        cilentId:
          type: "string"
        name:
          type: "string"
        description:
          $ref: '#/components/schemas/Description'
        price:
          type: "number"
        discontedPrice:
          type: "number"
        channelCount:
          type: "integer"
        entertaintment:
          type: "array"
          items:
            type: "string"
        addons:
          type: "array"
          items:
            $ref: '#/components/schemas/AddOn'
        images:
          type: "array"
          items:
            type: "string"
        termsAndConditions:
          type: "array"
          items:
            $ref: '#/components/schemas/TermsAndConditions'

    CallPackage:
      type: "object"
      properties:
        id:
          type: "string"
        cilentId:
          type: "string"
        name:
          type: "string"
        description:
          $ref: '#/components/schemas/Description'
        price:
          type: "number"
        discontedPrice:
          type: "number"
        numberOfMinutes:
          type: "integer"
        addons:
          type: "array"
          items:
            $ref: '#/components/schemas/AddOn'
        termsAndConditions:
          type: "array"
          items:
            $ref: '#/components/schemas/TermsAndConditions'

    Promotion:
      type: "object"
      properties:
        id:
          type: "string"
        cilentId:
          type: "string"
        promotionDetails:
          type: "array"
          items:
            type: "object"
            properties:
              name:
                type: "string"
              description:
                $ref: '#/components/schemas/Description'
        price:
          type: "number"
        discontedPrice:
          type: "number"
        createdDate:
          type: "string"
        endingDate:
          type: "string"
        images:
          type: "array"
          items:
            type: "string"
        termsAndConditions:
          type: "array"
          items:
            $ref: '#/components/schemas/TermsAndConditions'

    Package:
      type: "object"
      properties:
        id:
          type: "string"
        cilentId:
          type: "string"
        name:
          type: "string"
        description:
          $ref: '#/components/schemas/Description'
        price:
          type: "number"
        discontedPrice:
          type: "number"
        internetPackage:
          $ref: '#/components/schemas/InternetPackage'
        tvPackage:
          $ref: '#/components/schemas/TVPackage'
        callPackage:
          $ref: '#/components/schemas/CallPackage'
        addons:
          type: "array"
          items:
            $ref: '#/components/schemas/AddOn'
        images:
          type: "array"
          items:
            type: "string"
        termsAndConditions:
          type: "array"
          items:
            $ref: '#/components/schemas/TermsAndConditions'

    AvailablePackagesResponse:
      type: "object"
      properties:
        status:
          type: "string"
          example: "PT1"
        packages:
          type: "array"
          items:
            $ref: '#/components/schemas/Package'

    InstallationLocation:
      type: "object"
      properties:
        latitude:
          type: "string"
        longitude:
          type: "string"
        province:
          type: "string"
        provinceCode:
          type: "string"
        city:
          type: "string"
        cityCode:
          type: "string"
        district:
          type: "string"
        districtCode:
          type: "string"
        street:
          type: "string"
        streetCode:
          type: "string"
        postalCode:
          type: "string"
        rtrw:
          type: "string"
        addressDescription:
          type: "string"

  securitySchemes:
    BasicAuth:
      type: http
      scheme: basic
    BearerAuth:
      type: http
      scheme: bearer
      bearerFormat: JWT
