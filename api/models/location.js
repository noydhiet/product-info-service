/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Dulaj Pathirana on 27/12/2019, 3:45 PM
 */

const joi = require('@hapi/joi');

const locationSchema = joi.object({
    latitude: joi.string().required(),
    longitude: joi.string().required(),
    province: joi.string().required(),
    provinceCode: joi.string().required(),
    city: joi.string().required(),
    cityCode: joi.string().required(),
    district: joi.string().required(),
    districtCode: joi.string().required(),
    street: joi.string().allow('').required(),
    streetCode: joi.string().allow('').optional(),
    postalCode: joi.string().required(),
    rtrw: joi.string().allow('').optional(),
    addressDescription: joi.string().allow('').optional()
});


module.exports = {
    locationSchema
};
