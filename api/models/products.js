/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Dulaj Pathirana on 22/10/2019, 5:30 PM
 */

const Joi = require('@hapi/joi');

class Description {

    constructor(title, subtitle, image) {
        this.title = title;
        this.subtitle = subtitle;
        this.image = image;
    }
}

class TermsAndConditions {

    constructor(title, subtitle, image) {
        this.title = title;
        this.subtitle = subtitle;
        this.image = image;
    }
}


class Product {

    constructor(id, clientId, name, group, price, discountedPrice, description, termsAndConditions, images = []) {
        this.id = id;
        this.clientId = clientId;
        this.name = name;
        this.group = group;
        this.price = price;
        this.discountedPrice = discountedPrice;
        this.description = description;
        this.termsAndConditions = termsAndConditions;
        this.images = images;
    }
}

class Package extends Product {

    constructor(id, clientId, name, group, price, discountedPrice, description, termsAndConditions, images = []
        , addons = [], internetPackage = null, callPackage = null, tvPackage = null) {
        super(id, clientId, name, group, price, discountedPrice, description, termsAndConditions, images);
        this.addons = addons;
        this.internetPackage = internetPackage;
        this.callPackage = callPackage;
        this.tvPackage = tvPackage;
    }

}

class InternetPackage extends Product {

    constructor(id, clientId, name, group, price, discountedPrice, description, termsAndConditions, images = []
        , speed = 0, numberOfUsers = 0, entertainment = [], internetAddons = []) {
        super(id, clientId, name, group, price, discountedPrice, description, termsAndConditions, images);
        this.speed = speed;
        this.numberOfUsers = numberOfUsers;
        this.entertainment = entertainment;
        this.internetAddons = internetAddons;
    }
}

class CallPackage extends Product {


    constructor(id, clientId, name, group, price, discountedPrice, description, termsAndConditions, images = []
        , minutes = 0, callAddons = []) {
        super(id, clientId, name, group, price, discountedPrice, description, termsAndConditions, images);
        this.minutes = minutes;
        this.callAddons = callAddons;

    }
}

class TVPackage extends Product {

    constructor(id, clientId, name, group, price, discountedPrice, description, termsAndConditions, images = []
        , channels = [], tvAddons = []) {
        super(id, clientId, name, group, price, discountedPrice, description, termsAndConditions, images);
        this.channels = channels;
        this.tvAddons = tvAddons;
    }
}

class Addon extends Product {

    constructor(id, clientId, name, group, price, discountedPrice, description, termsAndConditions, images = []) {
        super(id, clientId, name, group, price, discountedPrice, description, termsAndConditions, images);

    }
}

class InternetAddon extends Addon {
    constructor(id, clientId, name, group, price, discountedPrice, description, termsAndConditions, images = []) {
        super(id, clientId, name, group, price, discountedPrice, description, termsAndConditions, images);

    }
}

class CallAddon extends Addon {
    constructor(id, clientId, name, group, price, discountedPrice, description, termsAndConditions, images = []) {
        super(id, clientId, name, group, price, discountedPrice, description, termsAndConditions, images);

    }
}


class TVAddon extends Addon {
    constructor(id, clientId, name, group, price, discountedPrice, description, termsAndConditions, images = []) {
        super(id, clientId, name, group, price, discountedPrice, description, termsAndConditions, images);

    }
}

const baseProductSchema = {
    id: Joi.string().required(),
    name: Joi.string().required(),
    description: Joi.string().allow('').required(),
    active: Joi.boolean().required(),
};

const internetPackageSchema = Joi.object({
    ...baseProductSchema,
    speed: Joi.number().required(),
    numberOfUsers: Joi.number().required(),
    entertainment: Joi.array().items(Joi.string().optional()).required(),
    addons: Joi.array().items(Joi.object().optional()).required(),
});

const tvPackageSchema = Joi.object({
    ...baseProductSchema,
    channels: Joi.array().items(Joi.object({
        group: Joi.string().required(),
        image: Joi.string().required(),
        description: Joi.string().allow('').required(),
        active: Joi.boolean().required(),
    }).optional()).required(),
    addons: Joi.array().items(Joi.object().optional()).required(),
});

const callPackageSchema = Joi.object({
    ...baseProductSchema,
    minutes: Joi.number().required(),
    addons: Joi.array().items(Joi.object().optional()).required(),
});

const tncSchema = Joi.object({});

const addonSchema = Joi.object({
    ...baseProductSchema,
    clientId: Joi.string().required(),
    group: Joi.string().required(),
    priority: Joi.number().required(),
    price: Joi.number().required(),
    discountedPrice: Joi.number().optional(),
    termsAndConditions: Joi.array().items(tncSchema.optional()).required(),
    images: Joi.array().items(Joi.string().optional()).required(),
});

const addonOutputProperties = [
    'id',
    'clientId',
    'name',
    'group',
    'price',
    'description',
    'images',
];

const packageSchema = Joi.object({
    ...baseProductSchema,
    clientId: Joi.string().required(),
    group: Joi.string().required(),
    price: Joi.number().required(),
    discountedPrice: Joi.number().optional(),
    termsAndConditions: Joi.array().items(tncSchema.optional()).required(),
    images: Joi.array().items(Joi.string().optional()).required(),
    addons: Joi.array().items(addonSchema.optional()).required(),
    internetPackage: internetPackageSchema.required(),
    callPackage: callPackageSchema.required(),
    tvPackage: tvPackageSchema.required(),
    clientMetadata: Joi.object().required(),
});

const packageOutputProperties = [
    'id',
    'clientId',
    'name',
    'group',
    'price',
    'discountedPrice',
    'description',
    'images',
    'internetPackage',
    'callPackage',
    'tvPackage'
];


module.exports = {
    Package,
    InternetPackage,
    TVPackage,
    CallPackage,
    Addon,
    InternetAddon,
    CallAddon,
    TVAddon,
    Description,
    TermsAndConditions,
    packageSchema,
    addonSchema,
    addonOutputProperties,
    packageOutputProperties
};
