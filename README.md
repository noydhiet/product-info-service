## Product Info Service

[![pipeline status](https://gitlab.mihpccw.com/client-services/product-info-service/badges/master/pipeline.svg)](https://gitlab.mihpccw.com/client-services/product-info-service/pipelines)
[![coverage report](https://gitlab.mihpccw.com/client-services/product-info-service/badges/master/coverage.svg)](https://gitlab.mihpccw.com/client-services/product-info-service/commits/master)

Configurations

Needs to setup the env for the local run. Sample dev env file is located.

Fixme: Change the URL to AWS SonarQube

[![Quality Gate Status](http://sonarqube-http-sonarqube.18.140.86.30.xip.io/api/project_badges/measure?project=myind%3Aclient-services%3Aproduct-info-service&metric=alert_status)](http://sonarqube-http-sonarqube.18.140.86.30.xip.io/dashboard?id=myind%3Aclient-services%3Aproduct-info-service)
[![Code Smells](http://sonarqube-http-sonarqube.18.140.86.30.xip.io/api/project_badges/measure?project=myind%3Aclient-services%3Aproduct-info-service&metric=code_smells)](http://sonarqube-http-sonarqube.18.140.86.30.xip.io/dashboard?id=myind%3Aclient-services%3Aproduct-info-service)
[![Vulnerabilities](http://sonarqube-http-sonarqube.18.140.86.30.xip.io/api/project_badges/measure?project=myind%3Aclient-services%3Aproduct-info-service&metric=vulnerabilities)](http://sonarqube-http-sonarqube.18.140.86.30.xip.io/dashboard?id=myind%3Aclient-services%3Aproduct-info-service)
[![Duplicated Lines (%)](http://sonarqube-http-sonarqube.18.140.86.30.xip.io/api/project_badges/measure?project=myind%3Aclient-services%3Aproduct-info-service&metric=duplicated_lines_density)](http://sonarqube-http-sonarqube.18.140.86.30.xip.io/dashboard?id=myind%3Aclient-services%3Aproduct-info-service)
[![Maintainability Rating](http://sonarqube-http-sonarqube.18.140.86.30.xip.io/api/project_badges/measure?project=myind%3Aclient-services%3Aproduct-info-service&metric=sqale_rating)](http://sonarqube-http-sonarqube.18.140.86.30.xip.io/dashboard?id=myind%3Aclient-services%3Aproduct-info-service)
[![Technical Debt](http://sonarqube-http-sonarqube.18.140.86.30.xip.io/api/project_badges/measure?project=myind%3Aclient-services%3Aproduct-info-service&metric=sqale_index)](http://sonarqube-http-sonarqube.18.140.86.30.xip.io/dashboard?id=myind%3Aclient-services%3Aproduct-info-service)
[![Lines of Code](http://sonarqube-http-sonarqube.18.140.86.30.xip.io/api/project_badges/measure?project=myind%3Aclient-services%3Aproduct-info-service&metric=ncloc)](http://sonarqube-http-sonarqube.18.140.86.30.xip.io/dashboard?id=myind%3Aclient-services%3Aproduct-info-service)

Start Service
```
npm start
```
To view the Swagger UI interface:

```
open http://localhost:8080/docs
```
